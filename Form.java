/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package regsosek;

import java.util.ArrayList;
/**
 *
 * @author Windows10
 */
public interface Form {
    void addVariabel();
    void inputData();
    boolean validasi();
    void reset();
    ArrayList<String> setPessanError();
    String setPesanError();
    void getPesanError();
}
