/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regsosek_yes;
/**
 *
 * @author Acer
 */
public class KodeWilayah {
    private int kodeProv;
    private int kodeKabKota;
    private int kodeKecamatan;
    private int kodeDesa;
    private int kodeSLS;
    
    public KodeWilayah(){
        
    }
    
    public KodeWilayah(int kodeProv, int kodeKabKota,int kodeKecamatan, int kodeDesa, int kodeSLS){
        this.kodeProv = kodeProv;
        this.kodeKabKota = kodeKabKota;
        this.kodeKecamatan = kodeKecamatan;
        this.kodeDesa = kodeDesa;
        this.kodeSLS = kodeSLS;
    }
    /**
    * @return the provinsi
    */
    public Integer getKodeProv() {
        return kodeProv;
    }

    /**
     * @param kodeProv
    * @throws IllegalArgumentException jika provinsi tidak berada di rentang 0-99
    */
    public void setKodeProv(Integer kodeProv) throws IllegalArgumentException {
        try {
            if (kodeProv < 1 || kodeProv > 99) {
                throw new IllegalArgumentException("input kode dalam range 1 - 99");
            }
            this.kodeProv = kodeProv;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
    * @return the kabKot
    */
    public Integer getKodeKabKota() {
        return kodeKabKota;
    }

    /**
    * @param kabKot the kabKot to set
    */
    public void setKodeKabKota(Integer kodeKabKota) throws IllegalArgumentException {
        try {
            if (kodeKabKota < 1 || kodeKabKota > 99) {
                throw new IllegalArgumentException("input kode dalam range 1 - 99");
            }
            this.kodeKabKota = kodeKabKota;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
    * @return the kecamatan
    */
    public Integer getKodeKecamatan() {
        return kodeKecamatan;
    }

    /**
    * @param kecamatan the kecamatan to set
    */
    public void setKodeKecamatan(Integer kodeKecamatan) throws IllegalArgumentException {
        try {
            if (kodeKecamatan< 1 || kodeKecamatan > 999) {
                throw new IllegalArgumentException("input kode dalam range 1 - 999");
            }
            this.kodeKecamatan = kodeKecamatan;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
    * @return the desaKel
    */
    public Integer getKodeDesa() {
        return kodeDesa;
    }

    /**
    * @param desaKel the desaKel to set
    */
    public void setKodeDesa(Integer kodeDesa) throws IllegalArgumentException {
        try {
            if (kodeDesa < 1 || kodeDesa > 999) {
                throw new IllegalArgumentException("input kode dalam range 1 - 999");
            }
            this.kodeDesa = kodeDesa;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    /**
    * @return the kodeSLS
    */
    public Integer getKodeSLS() {
        return kodeSLS;
    }

    /**
    * @param kodeSLS the kodeSLS to set
    */
    public void setKodeSLS(Integer kodeSLS)throws IllegalArgumentException {
        try {
            if (kodeSLS < 1 || kodeSLS > 9999) {
                throw new IllegalArgumentException("input kode dalam range 1 - 9999");
            }
            this.kodeSLS = kodeSLS;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}