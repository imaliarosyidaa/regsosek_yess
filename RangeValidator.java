package regsosek;

/**
 *
 * @author USER
 */
public class RangeValidator {
    public static boolean validateRange(int number, int min, int max) {
        try {
            // Mengecek apakah angka berada di dalam rentang yang diinginkan
            if (number < min || number > max) {
                return false;
            }

            return true;
        } catch (Exception e) {
            System.out.println("Terjadi kesalahan: " + e.getMessage());
            return false;
        }
    }
}

