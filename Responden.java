package regsosek;

/**
 *
 * @author Acer
 */
public class Responden extends Orang {
    private int umur;
    private int jenisKelamin;

    public Responden() {
    }

    public Responden(String nama, int role) {
        super(nama, role);
    }
    
    public Responden(int umur, int jenisKelamin) {
        this.umur = umur;
        this.jenisKelamin = jenisKelamin;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public int getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(int jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
}
