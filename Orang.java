package regsosek;

/**
 *
 * @author Acer
 */

public abstract class Orang {
    private String nama;
    private int role;

    public Orang() {}

    public Orang(String nama, int role) {
        this.nama = nama;
        this.role = role;
    }

    public void setName(String nama) {
        this.nama = nama;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getName() {
        return nama;
    }

    public String getRole() {
        return Integer.toString(role);
    }
}
