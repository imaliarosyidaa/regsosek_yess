package regsosek;

/**
 *
 * @author HP
 */
public class ART extends Responden {
    private int nomorUrut;
    private String NIK;
    private int statusHubungan;
    
    public ART(){
        
    }
    
    public ART(String nama, int role){
        super(nama, role);
    }
    
    public ART(int umur, int jenisKelamin){
        super(umur, jenisKelamin);
    }
    
    public void ART(int nomorUrut, String NIK, int statusHubungan){
        this.nomorUrut = nomorUrut;
        this.NIK = NIK;
        this.statusHubungan = statusHubungan;
    }
    
    public void setNomorUrut(int nomorUrut){
        this.nomorUrut = nomorUrut;
    }
    
    public void setNIK(String NIK){
        this.NIK = NIK;
    }
    
    public void statusHubungan(int statusHubungan){
        this.statusHubungan = statusHubungan;    
    }
    
    public int getNomorUrut(){
        return nomorUrut;
    }
    
    public String getNIK(){
        return NIK;
    }
    
    public String getStatusHubungan(){
        return Integer.toString(statusHubungan);
    }
}