/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regsosek_yes;
/**
 *
 * @author HP
 */
public class RumahTangga extends ART {
    private int kelompokKeluarga;
    private String namaKepala;
    private int noUrutKeluarga;
    
    public void RumahTangga(){}
    
    public void RumahTangga(int kelompokKeluarga, String namaKepala, int noUrutKeluaurga){
        this.kelompokKeluarga=kelompokKeluarga;
        this.namaKepala=namaKepala;
        this.noUrutKeluarga=noUrutKeluarga;
    }
    
    public void setKelKeluarga(Integer kelompokKeluarga) throws IllegalArgumentException {
        try {
            if (kelompokKeluarga < 1 || kelompokKeluarga > 11) {
                throw new IllegalArgumentException("input kode dalam range 1 - 11");
            }
            this.kelompokKeluarga = kelompokKeluarga;
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
            
    public void setNamaKepala(String namaKepala){
        this.namaKepala=namaKepala;
    }
    
    public void setNoUrutKeluarga(int NoUrut){
        this.noUrutKeluarga=NoUrut;
    }
    
    public String getKelompokKeluarga(){
        return Integer.toString(kelompokKeluarga);
    }
    
    public String getNamaKepala(){
        return namaKepala;
    }
}
