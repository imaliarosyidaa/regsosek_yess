## Deskripsi Mengenai UML Diagram

- Terdapat interface form berisi atribut blok pertanyaan di dalamnya terdapat method untuk menambahkan variabel, input data, validasi, reset, pesan error (exception)
- Terdapat class Blok I dan Blok IV yang meng implement interface form
- Interface form memiliki hubungan composition dengan class kuesioner, di mana kuesioner harus mengandung atribut form di dalamnya. Class Kuesioner memiliki method jumlah, reset, validate, dan save.
- Terdapat abstract class orang yang memiliki atribut nama dan role, memiliki method get dan set atribut private nama dan role.
- Kelas Petugas merupakan turunan dari abstract class orang
- Kelas Operator adalah turunan dari class petugas yang memiliki method entry data
- Kelas Admin merupakan turunan dari class petugas yang memiliki method monitoring
- Kelas responden merupakan turunan dari abstract class orang yang memiliki atribut umur dan jenis kelamin serta method get dan set untuk mengakses atribut yang bersifat private
- Kelas ART merupakan turunan dari kelas Responden, memiliki atribut nomorUrut, NIK, statusHubungan dengan method get dan set untuk mengakses atribut yang bersifat private. Selain itu, terdapat method untuk validasi umur di mana umur ART yang lebih dari lima tahun akan mendapatkan atribut tambahan yaitu , jenisGangguan, ijazah, statusKerja, lapUsaha, statusKedudukan, kepemilikanUsaha, jenisUsaha.
- Kelas Rumah Tangga memiliki hubugan komposisi dengan ART dan tempat artinya dalam rumah tangga pasti memiliki ART dan Tempat. Class rumah tangga memiliki atribut kelompok keluarga, namaKepalaRumahTangga, noUrutKeluarga, Tempat dan ART. Method di dalamnya adalah method get set untuk mengakses atribut yang bersifat private
- Kelas Tempat memiliki atribut kodeWilayah serta method tempat dan daftar kode wilayah. 
- Kelas KodeWilayah merupakan extend dari class tempat, berisi atribut kodeProv, kodeKabKota, kodeKecamatan, kodeDesa, kodeSLS
- Kelas Karakteristik Wilayah merupakan extend dari kelas tempat, memiliki atribut namaSLS, lokasi, noUrutBangunan, idWilkerstat serta method get dan set untuk mengakses atribut private