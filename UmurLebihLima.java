package regsosek;

/**
 *
 * @author Windows10
 */
public class UmurLebihLima extends ART{
    private int jenisGangguan;
    private int ijazah;
    private int lapUsaha;
    private int statusKedudukan;
    private int kepemilikanUsaha;
    private int jenisUsaha;
    private boolean isGangguan;
    private boolean isWork;
    
    public UmurLebihLima(){
        
    }
    
    public UmurLebihLima(String nama, int role){
        super(nama, role);
    }
    
    public UmurLebihLima(int umur, int jenisKelamin){
        super(umur, jenisKelamin);
    }

    public void setIjazah(int ijazah){
        this.ijazah = ijazah;
    }
    
    public int getIjazah(){
        return ijazah;
    }

    public void setLapUsaha(int lapUsaha){
        this.lapUsaha = lapUsaha;
    }
    
    public int getLapUsaha(){
        return lapUsaha;
    }

    public void setStatusKedudukan (int statusKedudukan){
        this.statusKedudukan = statusKedudukan;
    }
    
    public int getStatusKedudukan(int statusKedudukan){
        return statusKedudukan;
    }
    
    public void setKepemilikanUsaha (int kepemilikanUsaha){
        this.kepemilikanUsaha = kepemilikanUsaha;
    }
    
    public int getKepemilikanUsaha (){
        return kepemilikanUsaha;
    }

    public void setJenisUsaha (int jenisUsaha){
        this.jenisUsaha = jenisUsaha;
    }
    
    public int getJenisUsaha(){
        return jenisUsaha;
    }

    public boolean isGangguan(boolean isGangguan){
        isGangguan = true;
        return isGangguan;
    }
    
    public boolean isWork(boolean isWork){
        isWork = true;
        return isWork;
    }
}
