package regsosek_yess;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Regsosek Test Aplikasi Simulasi
 * @author Kelompok 5
 */
public class RegsosekMain {
    //String[] prov = {
        
    //};
    static void blok1(ArrayList<RumahTangga> rt, ArrayList<KodeWilayah> kw, ArrayList<Karakteristik_Wilayah> k_w){
        boolean quit = false;
        int i = 0;
        do{
            System.out.println("\t\tRepublik Indonesia");
            System.out.println("\tREGISTRASI SOSIAL EKONOMI 2022");
            System.out.println("PENDATAAN KELUARGA/PENDUDUK DI WILAYAH KHUSUS");
            System.out.println("===============================================");
            
            Scanner in = new Scanner(System.in);
            System.out.println("101. Provinsi: "); int kodeProv = in.nextInt();
            System.out.println("102. Kabupaten/Kota: "); int kodeKabKota = in.nextInt();
            System.out.println("103. Kecamatan: "); int kodeKecamatan = in.nextInt();
            System.out.println("104. Desa/Kelurahan: "); int kodeDesa = in.nextInt();
            System.out.println("105. Kode SLS/Non SLS: "); int kodeSLS = in.nextInt();
            System.out.println("106. Nama SLS/Non SLS: "); String namaSLS = in.nextLine();
            System.out.println("107. Lokasi Pendataan: "); String lokasi = in.nextLine();
            System.out.println("108. Kelompok Keluarga/Penduduk di Wilayah Khusus: "); int kelompokKeluarga = in.nextInt();
            System.out.println("109. Nama Kepala Keluarga: "); String namaKepala = in.nextLine();
            System.out.println("110. Nomor Urut Bangunan Tempat Tinggal: "); int noUrutBangunan = in.nextInt();
            System.out.println("111. Nomor Urut Keluarga: "); int noUrutKeluarga = in.nextInt();
            System.out.println("112. ID Landmark Wilkerstat: "); String idWilkerstat = in.nextLine();
            
            rt.add(new RumahTangga(kelompokKeluarga, namaKepala, noUrutKeluarga));
            kw.add(new KodeWilayah(kodeProv, kodeKabKota, kodeKecamatan, kodeDesa, kodeSLS));
            k_w.add(new Karakteristik_Wilayah(namaSLS, lokasi, noUrutBangunan, idWilkerstat));
            
            System.out.println("1. Lanjut Blok 4");
            System.out.println("2. Input data lagi");
            int ulang = in.nextInt();
            if(ulang == 1){
                quit = true;
                i = i++;
            }
        }
        while(!quit);
    }
    
    static void blok4(ArrayList<ART> art, ArrayList<UmurLebihLima> ull){
        boolean quit = false;
        int i = 0;
        do{
            Scanner in = new Scanner(System.in);
            System.out.println("Nomor Urut anggota Keluarga: "); int nomorUrut = in.nextInt();
            System.out.println("Nama: "); int nama = in.nextInt();
            System.out.println("NIK"); int NIK = in.nextInt();
            System.out.println("Jenis Kelamin: "); int jenisKelamin = in.nextInt();
            System.out.println("Status hubungan dengan kepala keluarga: "); int statusHubungan = in.nextInt();
            System.out.println("Umur: "); int umur = in.nextInt();
            System.out.println("Apakah memiliki kesulitan? ");
            System.out.println("Jenis kesulitan/gangguan?"); int jenisGangguan = in.nextInt();
            System.out.println("Ijazah/STTB tertinggi yang dimiliki"); int ijazah = in.nextInt();
            System.out.println("Apakah bekerja/membantu bekerja?");
            System.out.println("Lapangan usaha dari pekerjaan utama: "); int lapUsaha = in.nextInt();
            System.out.println("Status kedudukan dlaam pekerjaan utama(seminggu terakhir) "); int statusKedudukan = in.nextInt();
            System.out.println("Apakah memiliki usaha? 1. Ya 2. Tidak h"); int kepemilikanUsaha = in.nextInt();
            System.out.println("Jumalah usaha yang dimiliki: ");
            System.out.println("Lapangan usaha dari usaha utama (tuliskan selengkap-lengkapnya) (kode diisi oleh PML)");
            
            //art.add(new )
            
            System.out.println("1. Keluar");
            System.out.println("2. Input data lagi");
            int ulang2 = in.nextInt();
            if(ulang2 == 1){
                quit = true;
                i = i++;
            }
        } while(!quit);
        //for (int i=0;i<art.size();i++) {
        //    System.out.println(String.format("%d. [nama: %s]",i+1,art.get(i).getNIK()));
        //}
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<RumahTangga> rt = new ArrayList();
        ArrayList<KodeWilayah> kw = new ArrayList();
        ArrayList<Karakteristik_Wilayah> k_w = new ArrayList();
        blok1(rt,kw,k_w); // Memanggil blok1
        
        ArrayList<ART> art = new ArrayList();
        ArrayList<UmurLebihLima> ull = new ArrayList();
        blok4(art, ull); // Memanggil blok4
        
    }
    
}
