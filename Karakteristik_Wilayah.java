package regsosek;

/**
 *
 * @author afied
 */
public class Karakteristik_Wilayah {
    private String namaSLS;
    private String lokasi;
    private int noUrutBangunan;
    private String idWilkerstat;
    
    public Karakteristik_Wilayah(){
        
    }
    
    public Karakteristik_Wilayah(String namaSLS, String lokasi, int noUrutBangunan, String idWilkerstat){
        this.namaSLS = namaSLS;
        this.lokasi = lokasi;
        this.noUrutBangunan = noUrutBangunan;
        this.idWilkerstat = idWilkerstat;
    }
    
    public void setNamaSLS(String namaSLS){
        this.namaSLS = namaSLS;
    }
    
    public void setLokasi(String lokasi){
        this.lokasi = lokasi;
    }
    
    public void setNoUrutBangunan(int noUrutBangunan){
        this.noUrutBangunan = noUrutBangunan;
    }
    
    public void setIdWilkerstat(String idWilkerstat){
        this.idWilkerstat = idWilkerstat;
    }
    
    public String getNamaSLS(){
        return namaSLS;
    }
    
    public String getLokasi(){
        return lokasi;
    }
    
    public int getNoUrutBangunan(){
        return noUrutBangunan;
    }
    
    public String getIdWilkerstat(){
        return idWilkerstat;
    }
}
