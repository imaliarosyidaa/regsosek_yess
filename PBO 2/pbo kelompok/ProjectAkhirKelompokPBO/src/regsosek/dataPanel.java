/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package regsosek;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import regsosek.editPanel;

/**
 *
 * 
 */
public class dataPanel extends javax.swing.JPanel {
    private final JScrollPane contentScrollPane;
    /**
     * Creates new form dataPanel
     */
    public dataPanel(JScrollPane contentScrollPane) {
        this.contentScrollPane = contentScrollPane;
        initComponents();
        loadTableData();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        title2Label = new javax.swing.JLabel();
        categoryComboBox = new javax.swing.JComboBox<>();
        searchTextField = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        dataTable = new javax.swing.JTable();
        lihatButton = new javax.swing.JButton();
        editButton = new javax.swing.JButton();
        infoLabel = new javax.swing.JLabel();
        info2Label = new javax.swing.JLabel();

        setBackground(new java.awt.Color(206, 214, 224));

        titleLabel.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        titleLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleLabel.setText("Data Kuesioner Blok. I");

        title2Label.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        title2Label.setText("Cari berdasarkan");

        categoryComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Kode Provinsi", "Kode Kabupaten/Kota", "Kode KJU", "No Urut", "Bentuk Badan Usaha", "Jenis Usaha Utama", "Status Pencacahan", "Status Perusahaan" }));
        categoryComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoryComboBoxActionPerformed(evt);
            }
        });

        searchTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchTextFieldActionPerformed(evt);
            }
        });

        searchButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        searchButton.setText("Cari");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        dataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id Rumah Tangga*", "Kode Provinsi", "Kode Kab/Kota", "Kode Kecamatan", "Kode Desa", "Kode SLS", "Nama SLS", "Lokasi Pendataan", "Kelompok Keluarga", "No. Urut Bangunan", "No. Urut Keluarga", "Id Landmark"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(dataTable);

        lihatButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lihatButton.setText("Lihat");
        lihatButton.setPreferredSize(new java.awt.Dimension(61, 35));
        lihatButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lihatButtonActionPerformed(evt);
            }
        });

        editButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        editButton.setText("Edit");
        editButton.setPreferredSize(new java.awt.Dimension(55, 35));
        editButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editButtonActionPerformed(evt);
            }
        });

        infoLabel.setText("* Id Perusahaan terdiri dari Kode Provinsi, Kode Kab/Kota, Kode Kecamatan, Kode KJU, dan Nomor Urut");

        info2Label.setText("Silahkan pilih baris pada tabel terlebih dahulu. Klik Lihat untuk menampilkan isian dan  Edit untuk mengubah isian");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lihatButton, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editButton, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(infoLabel)
                            .addComponent(info2Label)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(searchTextField)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(searchButton, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(title2Label)
                            .addGap(18, 18, 18)
                            .addComponent(categoryComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(4, 4, 4)))
                .addGap(32, 32, 32))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(title2Label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(categoryComboBox))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchTextField)
                    .addComponent(searchButton, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(infoLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(info2Label))
                    .addComponent(lihatButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchButtonActionPerformed

    private void categoryComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoryComboBoxActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_categoryComboBoxActionPerformed

    private void lihatButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lihatButtonActionPerformed
        // TODO add your handling code here:
        int row = dataTable.getSelectedRow();
        if (row >= 0){
            String idPer = dataTable.getValueAt(row, 0).toString();
            contentScrollPane.setViewportView(new viewPanel(contentScrollPane, idPer));
        }
        else {
            JOptionPane.showMessageDialog(this, "Gagal menampilkan data! Silahkan pilih baris dalam tabel terlebih dulu", "Gagal", JOptionPane.ERROR_MESSAGE);
        }      
        
    }//GEN-LAST:event_lihatButtonActionPerformed

    private void searchTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchTextFieldActionPerformed
        // TODO add your handling code here:
     
    }//GEN-LAST:event_searchTextFieldActionPerformed

    private void editButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editButtonActionPerformed
        // TODO add your handling code here:
         int row = dataTable.getSelectedRow();
        if (row >= 0){
            String idPer = dataTable.getValueAt(row, 0).toString();
             try {
                 contentScrollPane.setViewportView(new editPanel(contentScrollPane, idPer));
             } catch (SQLException ex) {
                 Logger.getLogger(dataPanel.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
        else {
            JOptionPane.showMessageDialog(this, "Gagal dalam edit, silakan pilih baris terlebih dahulu!", "Gagal", JOptionPane.ERROR_MESSAGE);
        }  
    }//GEN-LAST:event_editButtonActionPerformed

    private void loadTableData(){ 
        DefaultTableModel dtm = (DefaultTableModel) dataTable.getModel();
        
        //refresh tabel 
        while(dtm.getRowCount()>0){
            dtm.removeRow(0);
        } 
        
        try{
            //isi tabel 
            for(idRumahTangga rt: Database.getInstance().selectidRumahTangga()){ 
                dtm.addRow(new Object[]{rt.getIdRuta(),rt.getKodeProv(),rt.getKodeKabKota(),rt.getKodeKec(),rt.getKodeDesa(),rt.getKodeSLS(),rt.getNamaSLS(),rt.getLokasiPendataan(),rt.getKelompokKeluarga(), rt.getNoUrutBangunan(), rt.getNoUrutKeluarga(), rt.getIdLandmark()}); 
            } 
        } catch (SQLException ex) { 
            System.err.println(ex); 
            JOptionPane.showMessageDialog(this, "Gagal menampilkan data", "Gagal", JOptionPane.ERROR_MESSAGE); 
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> categoryComboBox;
    private javax.swing.JTable dataTable;
    private javax.swing.JButton editButton;
    private javax.swing.JLabel info2Label;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton lihatButton;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JLabel title2Label;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables
}
