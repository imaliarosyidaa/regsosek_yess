/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regsosek;

/**
 *
 * @author Acer
 */
public class umurLebihLima {
    private String gangguan;
    private String jenisGangguan;
    private String ijazah;
    private String statusKerja;
    private String lapUsaha;
    private String statusKedudukan;
    private String kepemilikanUsaha;
    private String jumlahUsaha;
    private String lapUsahaUtama;

    public String getGangguan() {
        return gangguan;
    }

    public void setGangguan(String gangguan) {
        this.gangguan = gangguan;
    }

    public String getJenisGangguan() {
        return jenisGangguan;
    }

    public void setJenisGangguan(String jenisGangguan) {
        this.jenisGangguan = jenisGangguan;
    }

    public String getIjazah() {
        return ijazah;
    }

    public void setIjazah(String ijazah) {
        this.ijazah = ijazah;
    }

    public String getStatusKerja() {
        return statusKerja;
    }

    public void setStatusKerja(String statusKerja) {
        this.statusKerja = statusKerja;
    }

    public String getLapUsaha() {
        return lapUsaha;
    }

    public void setLapUsaha(String lapUsaha) {
        this.lapUsaha = lapUsaha;
    }

    public String getStatusKedudukan() {
        return statusKedudukan;
    }

    public void setStatusKedudukan(String statusKedudukan) {
        this.statusKedudukan = statusKedudukan;
    }

    public String getKepemilikanUsaha() {
        return kepemilikanUsaha;
    }

    public void setKepemilikanUsaha(String kepemilikanUsaha) {
        this.kepemilikanUsaha = kepemilikanUsaha;
    }

    public String getJumlahUsaha() {
        return jumlahUsaha;
    }

    public void setJumlahUsaha(String jumlahUsaha) {
        this.jumlahUsaha = jumlahUsaha;
    }

    public String getLapUsahaUtama() {
        return lapUsahaUtama;
    }

    public void setLapUsahaUtama(String lapUsahaUtama) {
        this.lapUsahaUtama = lapUsahaUtama;
    }


}
