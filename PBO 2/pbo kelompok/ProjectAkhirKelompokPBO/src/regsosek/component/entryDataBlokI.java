package regsosek.component;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import regsosek.Database;
import regsosek.Database;
import regsosek.MainFrame;
import regsosek.MainFrame;
import regsosek.idRumahTangga;
import regsosek.idRumahTangga;

public class entryDataBlokI extends javax.swing.JPanel {

    /**
     * Creates new form entryPanel
     */
    public entryDataBlokI() {
        initComponents();
        try {
            clearForm();
            //validateNama.setVisible(false);
            updateProvComb();
            
            //updateProvComb1();
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void updateProvComb() throws SQLException{
        String sql = "select * from provinsi";
        Connection conn = Database.getInstance().getConnection();
        try{
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while(rs.next()){
                kodeProv.addItem(rs.getString("nama_prov"));
            }
        }catch(SQLException e){
            throw e;
        }
    }
    
    public void updateKabKotComb() throws SQLException{
        String sql = "select * from kab_kota WHERE kode_prov = ?";
        Connection conn = Database.getInstance().getConnection();
        try{
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, kodeProv.getSelectedItem().toString().substring(0, 2));
            ResultSet rs = pst.executeQuery();
            kodeKabKota.removeAllItems();
            kodeKabKota.addItem("--- Kabupaten/Kota ---");
            while(rs.next()){
                kodeKabKota.addItem(rs.getString("nama_kab_kota"));
            }
        }catch(SQLException e){
            throw e;
        }
    }
    
    public void updateKecComb() throws SQLException{
        String sql = "select * from kecamatan WHERE kode_kab_kota = ?";
        Connection conn = Database.getInstance().getConnection();
        try{
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, kodeKabKota.getSelectedItem().toString().substring(0, 4));
            ResultSet rs = pst.executeQuery();
            kodeKec.removeAllItems();
            kodeKec.addItem("--- Kecamatan ---");
            while(rs.next()){
                kodeKec.addItem(rs.getString("nama_kec"));
            }
        }catch(SQLException e){
            throw e;
        }
    }
    
    private void clearForm(){ 
        namaSLS.setText("");
        kodeSLS.setText("");
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupTanamanPangan = new javax.swing.ButtonGroup();
        buttonGroupHortikultura = new javax.swing.ButtonGroup();
        buttonGroupPeternakan = new javax.swing.ButtonGroup();
        buttonGroupKehutanan = new javax.swing.ButtonGroup();
        buttonGroupPerikanan = new javax.swing.ButtonGroup();
        buttonGroupKunjungan = new javax.swing.ButtonGroup();
        pnlEntry = new javax.swing.JPanel();
        inputDataLabel = new javax.swing.JLabel();
        kirimButton = new javax.swing.JButton();
        jLabel27 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        kodeProv = new javax.swing.JComboBox<>();
        kodeKabKota = new javax.swing.JComboBox<>();
        kodeKec = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        kodeDesa = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        kodeSLS = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        namaSLS = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        lokasiPendataan = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        kelompokKeluarga = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        noUrutKeluarga = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        noUrutBangunan = new javax.swing.JTextField();
        idLandmark = new javax.swing.JTextField();

        inputDataLabel.setFont(new java.awt.Font("Montserrat", 1, 36)); // NOI18N
        inputDataLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        inputDataLabel.setText("INPUT DATA");

        kirimButton.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        kirimButton.setText("KIRIM");
        kirimButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kirimButtonActionPerformed(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel27.setText("BLOK I. KETERANGAN TEMPAT ");

        jLabel1.setText("Provinsi");

        jLabel2.setText("Kab Kota");

        jLabel3.setText("Kecamatan");

        kodeProv.setToolTipText("");

        kodeKabKota.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        kodeKabKota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kodeKabKotaActionPerformed(evt);
            }
        });

        kodeKec.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        kodeKec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kodeKecActionPerformed(evt);
            }
        });

        jLabel4.setText("Desa");

        kodeDesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kodeDesaActionPerformed(evt);
            }
        });

        jLabel5.setText("Kode SLS");

        kodeSLS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kodeSLSActionPerformed(evt);
            }
        });

        jLabel6.setText("Nama SLS");

        namaSLS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaSLSActionPerformed(evt);
            }
        });

        jLabel7.setText("Lokasi");

        lokasiPendataan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lokasiPendataanActionPerformed(evt);
            }
        });

        jLabel8.setText("Kelompok");

        kelompokKeluarga.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01. Apartemen", "02. Barak Militer", "03. Pesantren / Sejenis ", "04. Panti Asuhan ", "05. Rumah Sakit Jiwa", "06. Pengungsi ", "07. Penghuni Lapas", "08. Awak Kapal ", "09. Penghuni Perahu ", "10. Tunawisma ", "11. Lainnya", " " }));

        jLabel9.setText("No Urut Keluarga");

        jLabel10.setText("NoUrutBangunan");

        jLabel11.setText("IdLandmark");

        noUrutBangunan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noUrutBangunanActionPerformed(evt);
            }
        });

        idLandmark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                idLandmarkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kodeKabKota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kodeKec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(kodeProv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                        .addComponent(kelompokKeluarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lokasiPendataan)
                            .addComponent(namaSLS)
                            .addComponent(kodeSLS)
                            .addComponent(kodeDesa, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(idLandmark, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                            .addComponent(noUrutBangunan)
                            .addComponent(noUrutKeluarga))))
                .addContainerGap(133, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kodeProv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(kodeKabKota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(kodeKec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(kodeDesa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(kodeSLS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(namaSLS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lokasiPendataan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(kelompokKeluarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(noUrutKeluarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(noUrutBangunan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(idLandmark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        javax.swing.GroupLayout pnlEntryLayout = new javax.swing.GroupLayout(pnlEntry);
        pnlEntry.setLayout(pnlEntryLayout);
        pnlEntryLayout.setHorizontalGroup(
            pnlEntryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEntryLayout.createSequentialGroup()
                .addContainerGap(122, Short.MAX_VALUE)
                .addGroup(pnlEntryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27)
                    .addGroup(pnlEntryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEntryLayout.createSequentialGroup()
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEntryLayout.createSequentialGroup()
                            .addComponent(kirimButton, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(249, 249, 249))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlEntryLayout.createSequentialGroup()
                            .addComponent(inputDataLabel)
                            .addGap(144, 144, 144)))))
        );
        pnlEntryLayout.setVerticalGroup(
            pnlEntryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEntryLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(inputDataLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(kirimButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlEntry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 62, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnlEntry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 104, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void kirimButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kirimButtonActionPerformed
        // TODO add your handling code here:
        idRumahTangga rt = new idRumahTangga();
        rt.setKodeProv(kodeProv.getSelectedItem().toString().substring(2, 4));
        rt.setKodeKabKota(kodeKabKota.getSelectedItem().toString().substring(2, 4));
        rt.setKodeKec(kodeKec.getSelectedItem().toString().substring(4, 6));
        rt.setKodeDesa(kodeDesa.getText());
        rt.setKodeSLS(kodeSLS.getText());
        rt.setNamaSLS(namaSLS.getText());
        rt.setLokasiPendataan(lokasiPendataan.getText());
        rt.setKelompokKeluarga(kelompokKeluarga.getSelectedItem().toString().substring(1, 6));
        rt.setNoUrutBangunan( noUrutBangunan.getText());
        rt.setNoUrutKeluarga(noUrutKeluarga.getText());
        rt.setIdLandmark(idLandmark.getText());

        try {
            Database.getInstance().insertRuta(rt);
            clearForm();
            JOptionPane.showMessageDialog(this, "Data Berhasil Tersimpan");
        } catch (SQLException ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(this, "Lengkapi Semua Data", "Data Gagal Tersimpan", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_kirimButtonActionPerformed

    private void namaSLSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaSLSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaSLSActionPerformed

    private void kodeSLSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kodeSLSActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kodeSLSActionPerformed

    private void kodeDesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kodeDesaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kodeDesaActionPerformed

    private void kodeKecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kodeKecActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kodeKecActionPerformed

    private void noUrutBangunanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noUrutBangunanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_noUrutBangunanActionPerformed

    private void lokasiPendataanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lokasiPendataanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lokasiPendataanActionPerformed

    private void idLandmarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_idLandmarkActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_idLandmarkActionPerformed

    private void kodeKabKotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kodeKabKotaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kodeKabKotaActionPerformed
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupHortikultura;
    private javax.swing.ButtonGroup buttonGroupKehutanan;
    private javax.swing.ButtonGroup buttonGroupKunjungan;
    private javax.swing.ButtonGroup buttonGroupPerikanan;
    private javax.swing.ButtonGroup buttonGroupPeternakan;
    private javax.swing.ButtonGroup buttonGroupTanamanPangan;
    private javax.swing.JTextField idLandmark;
    private javax.swing.JLabel inputDataLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JComboBox<String> kelompokKeluarga;
    private javax.swing.JButton kirimButton;
    private javax.swing.JTextField kodeDesa;
    private javax.swing.JComboBox<String> kodeKabKota;
    private javax.swing.JComboBox<String> kodeKec;
    private javax.swing.JComboBox<String> kodeProv;
    private javax.swing.JTextField kodeSLS;
    private javax.swing.JTextField lokasiPendataan;
    private javax.swing.JTextField namaSLS;
    private javax.swing.JTextField noUrutBangunan;
    private javax.swing.JTextField noUrutKeluarga;
    private javax.swing.JPanel pnlEntry;
    // End of variables declaration//GEN-END:variables
}
