/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package regsosek;

/**
 */
public class idRumahTangga {

    public static void setText(String string) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    private String idRuta;
    private String kodeProv;
    private String kodeKabKota;
    private String kodeKec;
    private String kodeDesa;
    private String kodeSLS;
    private String namaSLS;
    private String lokasiPendataan;
    private String kelompokKeluarga;
    private String noUrutBangunan;
    private String noUrutKeluarga;
    private String idLandmark;

    public String getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(String idRuta) {
        this.idRuta = idRuta;
    }

    public String getKodeProv() {
        return kodeProv;
    }

    public void setKodeProv(String kodeProv) {
        this.kodeProv = kodeProv;
    }

    public String getKodeKabKota() {
        return kodeKabKota;
    }

    public void setKodeKabKota(String kodeKabKota) {
        this.kodeKabKota = kodeKabKota;
    }

    public String getKodeKec() {
        return kodeKec;
    }

    public void setKodeKec(String kodeKec) {
        this.kodeKec = kodeKec;
    }

    public String getKodeDesa() {
        return kodeDesa;
    }

    public void setKodeDesa(String kodeDesa) {
        this.kodeDesa = kodeDesa;
    }

    public String getKodeSLS() {
        return kodeSLS;
    }

    public void setKodeSLS(String kodeSLS) {
        this.kodeSLS = kodeSLS;
    }

    public String getNamaSLS() {
        return namaSLS;
    }

    public void setNamaSLS(String namaSLS) {
        this.namaSLS = namaSLS;
    }

    public String getLokasiPendataan() {
        return lokasiPendataan;
    }

    public void setLokasiPendataan(String lokasiPendataan) {
        this.lokasiPendataan = lokasiPendataan;
    }

    public String getKelompokKeluarga() {
        return kelompokKeluarga;
    }

    public void setKelompokKeluarga(String kelompokKeluarga) {
        this.kelompokKeluarga = kelompokKeluarga;
    }

    public String getNoUrutBangunan() {
        return noUrutBangunan;
    }

    public void setNoUrutBangunan(String noUrutBangunan) {
        this.noUrutBangunan = noUrutBangunan;
    }

    public String getNoUrutKeluarga() {
        return noUrutKeluarga;
    }

    public void setNoUrutKeluarga(String noUrutKeluarga) {
        this.noUrutKeluarga = noUrutKeluarga;
    }

    public String getIdLandmark() {
        return idLandmark;
    }

    public void setIdLandmark(String idLandmark) {
        this.idLandmark = idLandmark;
    }
}