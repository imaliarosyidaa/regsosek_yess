package regsosek;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import regsosek.anggotaKeluarga;
import regsosek.idRumahTangga;
import regsosek.umurLebihLima;
import regsosek.user;



/**
 */


public class Database implements Serializable{
    public static Database instance;
    
    private final String DB_TYPE = "mysql";
    private final String DB_HOST = "localhost";
    private final String DB_PORT = "3306";
    private final String DB_NAME = "regsosek";
    private final String DB_USER = "root";
    private final String DB_PASS = "";
    
    public Database(){   
    }
    
    public static synchronized Database getInstance(){
        if(instance == null){
            instance = new Database();
        }
        return instance;
    }
    
    public void insertRuta(idRumahTangga rumahtangga)throws SQLException{
        Connection conn = getConnection();
        try{
            String sql = "INSERT INTO rumahtangga VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);


//          idPer = kode unik dari setpuiap rumahtangga yang ada (gabungan dari beberapa kode yang ada pada KIP [refer ke kelas idRumahTangga])
            pstmt.setString(1, rumahtangga.getKodeProv());
            pstmt.setString(2, rumahtangga.getKodeKabKota());
            pstmt.setString(3, rumahtangga.getKodeKec());
            pstmt.setString(4, rumahtangga.getKodeDesa());
            pstmt.setString(5, rumahtangga.getKodeSLS());
            pstmt.setString(6, rumahtangga.getNamaSLS());
            pstmt.setString(7, rumahtangga.getLokasiPendataan());
            pstmt.setString(8, rumahtangga.getKelompokKeluarga());
            pstmt.setString(9, rumahtangga.getNoUrutBangunan());
            pstmt.setString(10, rumahtangga.getNoUrutKeluarga());
            pstmt.setString(11, rumahtangga.getIdLandmark());
            
            pstmt.executeUpdate();
        } catch(SQLException ex){
            throw ex;
        } finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public void insertAnggotaKeluarga(anggotaKeluarga anggotakeluarga)throws SQLException{
        Connection conn = getConnection();
        try{
            String sql = "INSERT INTO anggotakeluarga VALUES(?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);

//          idPer = kode unik dari setpuiap rumahtangga yang ada (gabungan dari beberapa kode yang ada pada KIP [refer ke kelas idRumahTangga])
            pstmt.setString(1, anggotakeluarga.getNoUrut());
            pstmt.setString(2, anggotakeluarga.getNama());
            pstmt.setString(3, anggotakeluarga.getNik());
            pstmt.setString(4, anggotakeluarga.getJenisKelamin());
            pstmt.setString(5, anggotakeluarga.getStatusHubungan());
            pstmt.setString(6, anggotakeluarga.getUmur());
            
            pstmt.executeUpdate();
        } catch(SQLException ex){
            throw ex;
        } finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public void insertUmurLebihLima(umurLebihLima umurlebihlima)throws SQLException{
        Connection conn = getConnection();
        try{
            String sql = "INSERT INTO umurlebihlima VALUES(?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);


//          idPer = kode unik dari setpuiap rumahtangga yang ada (gabungan dari beberapa kode yang ada pada KIP [refer ke kelas idRumahTangga])
            pstmt.setString(1, umurlebihlima.getGangguan());
            pstmt.setString(2, umurlebihlima.getJenisGangguan());
            pstmt.setString(3, umurlebihlima.getIjazah());
            pstmt.setString(4, umurlebihlima.getStatusKerja());
            pstmt.setString(5, umurlebihlima.getLapUsaha());
            pstmt.setString(6, umurlebihlima.getStatusKedudukan());
            pstmt.setString(7, umurlebihlima.getKepemilikanUsaha());
            pstmt.setString(8, umurlebihlima.getJumlahUsaha());
            pstmt.setString(9, umurlebihlima.getLapUsahaUtama());
            
            pstmt.executeUpdate();
        } catch(SQLException ex){
            throw ex;
        } finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public void deleteRumahTangga(String idPer) throws SQLException{
        Connection conn = getConnection();
        try{
//            delete row RumahTangga berdasarkan id 
            String sql = "DELETE FROM rumahtangga WHERE id_rumahtangga = " +idPer;
            PreparedStatement pstmt = conn.prepareStatement(sql);
            
            pstmt.setString(1, idPer);
            pstmt.executeUpdate();
        }catch(SQLException ex){
            throw ex;
        }
        finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public List<idRumahTangga> selectidRumahTangga() throws SQLException{
//      create a List of RumahTangga 
        List<idRumahTangga> rutaList = new ArrayList<>();
       
        Connection conn = getConnection();
        
        try{
            String sql = "SELECT * FROM rumahtangga";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
//              dibutuhkan constructor kosong pada kelas idRumahTangga
                idRumahTangga idRuta = new idRumahTangga();
                idRuta.setKodeProv(rs.getString("kode_prov"));
                idRuta.setKodeKabKota(rs.getString("kode_kab_kota"));
                idRuta.setKodeKec(rs.getString("kode_kec")); // nanti (semua var) dipastikan lagi mengenai penamaan variabel di databasenya
                idRuta.setKodeDesa(rs.getString("kode_desa"));
                idRuta.setKodeSLS(rs.getString("kode_SLS"));
                idRuta.setNamaSLS(rs.getString("nama_SLS"));
                idRuta.setLokasiPendataan(rs.getString("lokasi_pendataan"));
                idRuta.setKelompokKeluarga(rs.getString("kelompok_keluarga"));
                idRuta.setNoUrutBangunan(rs.getString("no_urut_bangunan"));
                idRuta.setNoUrutKeluarga(rs.getString("no_urut_keluarga"));
                idRuta.setIdLandmark(rs.getString("idLandmark"));
               
                // menambahkan satu baris idRumahTangga beserta keseluruhan var nya ke dalam satu row pada List rumahtangga
                // nanti bisa ditampilkan di tabel, pakai ini 
                rutaList.add(idRuta);
            }// end of checking while if rs.next available
        } // end of try
        catch(SQLException ex){
            throw ex;
        }
        
//        finally{
//            if (conn != null){
//                conn.close();
//            }
//        }
        return rutaList;
    }
    
    public List<anggotaKeluarga> selectanggotaKeluarga() throws SQLException{
//      create a List of RumahTangga 
        List<anggotaKeluarga> artList = new ArrayList<>();
       
        Connection conn = getConnection();
        
        try{
            String sql = "SELECT * FROM anggotakeluarga";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
//              dibutuhkan constructor kosong pada kelas anggotaKeluarga
                anggotaKeluarga art = new anggotaKeluarga();
                art.setNoUrut(rs.getString("noUrut"));
                art.setNama(rs.getString("nama"));
                art.setNik(rs.getString("nik")); // nanti (semua var) dipastikan lagi mengenai penamaan variabel di databasenya
                art.setStatusHubungan(rs.getString("statusHubungan"));
                art.setUmur(rs.getString("umur"));
               
                // menambahkan satu baris anggotaKeluarga beserta keseluruhan var nya ke dalam satu row pada List anggotakeluarga
                // nanti bisa ditampilkan di tabel, pakai ini 
                artList.add(art);
            }// end of checking while if rs.next available
        } // end of try
        catch(SQLException ex){
            throw ex;
        }
        
//        finally{
//            if (conn != null){
//                conn.close();
//            }
//        }
        return artList;
    }
    
    public List<umurLebihLima> selectumurLebihLima() throws SQLException{
//      create a List of RumahTangga 
        List<umurLebihLima> umurlebihlimaList = new ArrayList<>();
       
        Connection conn = getConnection();
        
        try{
            String sql = "SELECT * FROM umurlebihlima";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
//              dibutuhkan constructor kosong pada kelas anggotaKeluarga
                umurLebihLima umurlebihlima = new umurLebihLima();
                umurlebihlima.setJenisGangguan(rs.getString("jenis_gangguan"));
                umurlebihlima.setIjazah(rs.getString("ijazah"));
                umurlebihlima.setStatusKerja(rs.getString("status_kerja")); // nanti (semua var) dipastikan lagi mengenai penamaan variabel di databasenya
                umurlebihlima.setLapUsaha(rs.getString("lapangan_usaha"));
                umurlebihlima.setStatusKedudukan(rs.getString("status_kedudukan"));
                umurlebihlima.setJumlahUsaha(rs.getString("jumlah_usaha"));
                umurlebihlima.setStatusKedudukan(rs.getString("status_kedudukan"));
                umurlebihlima.setLapUsahaUtama(rs.getString("lapangan_usaha_utama"));
                // menambahkan satu baris anggotaKeluarga beserta keseluruhan var nya ke dalam satu row pada List anggotakeluarga
                // nanti bisa ditampilkan di tabel, pakai ini 
                umurlebihlimaList.add(umurlebihlima);
            }// end of checking while if rs.next available
        } // end of try
        catch(SQLException ex){
            throw ex;
        }
        
//        finally{
//            if (conn != null){
//                conn.close();
//            }
//        }
        return umurlebihlimaList;
    }
    
    public void editRumahTangga(idRumahTangga idRumahTangga, String idPer) throws SQLException {
        Connection conn = getConnection();
        try{
            String sql = "UPDATE rumahtangga SET "
                    + "kode_prov = ?,"
                    + "kode_kab_kota = ?,"
                    + "kode_kec = ?,"
                    + "kode_desa = ?,"
                    + "kode_SLS = ?,"
                    + "nama_SLS = ?,"
                    + "lokasi_pendataan = ?,"
                    + "kelompok_keluarga = ?,"
                    + "no_urut_bangunan = ?,"
                    + "no_urut_keluarga = ?,"
                    + "idLandmark = ? WHERE "
                    + "id_rumahtangga = ?";
            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, idRumahTangga.getKodeProv());
            pstmt.setString(2, idRumahTangga.getKodeKabKota());
            pstmt.setString(3, idRumahTangga.getKodeKec());
            pstmt.setString(4, idRumahTangga.getKodeDesa());
            pstmt.setString(5, idRumahTangga.getKodeSLS());
            pstmt.setString(6, idRumahTangga.getNamaSLS());
            pstmt.setString(7, idRumahTangga.getLokasiPendataan());
            pstmt.setString(8, idRumahTangga.getKelompokKeluarga());
            pstmt.setString(9, idRumahTangga.getNoUrutBangunan());
            pstmt.setString(10, idRumahTangga.getNoUrutKeluarga());
            pstmt.setString(11, idRumahTangga.getIdLandmark());
            
            
            pstmt.executeUpdate();
        } catch (SQLDataException ex){
            throw ex;
        }
        finally{
            if(conn != null){
                conn.close();
            }
        }
        
    }
    
    public user getUser(String username)throws SQLException{
        user user = new user();
        Connection conn = getConnection();
        try{
            String sql = "SELECT * FROM `user` WHERE `username` = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, username);
            
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex){
            throw ex;
        } finally{
            if ((conn != null)) {
                conn.close();
            }
        }
        return user;
    }
    
    public void insertNewUser(user usr)throws SQLException{
        Connection conn = getConnection();
        try{
            String sql = "INSERT INTO user VALUES(?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, usr.getEmail());
            pstmt.setString(2, usr.getUsername());
            pstmt.setString(3, usr.getPassword());
            
            pstmt.executeUpdate();
        } catch (SQLException ex){
            throw ex;
        } finally{
            if ((conn != null)) {
                conn.close();
            }
        }
    }
    
    public Connection getConnection() throws SQLException{
        return DriverManager.getConnection("jdbc:"+DB_TYPE+"://"+DB_HOST+":"+DB_PORT+"/"+DB_NAME, DB_USER,DB_PASS);
        
    }

    
}// end of class
