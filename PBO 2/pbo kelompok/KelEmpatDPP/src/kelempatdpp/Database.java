package kelempatdpp;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Anggota Kelompok:
 * 1. Anak Agung Gede Rai Bhaskara Darmawan Pemayun (222011343 / 08)
 * 2. Muhamad Feriyanto                             (222011347 / 09)
 * 3. Niken Yuliana                                 (222011685 / 30)
 * 4. Triana Nuramalia Shalli                       (222011703 / 31)
 * 5. Muhammad Naufal Faishal                       (222011739 / 33)
 * 
 * @author MUHAMAD FERIYANTO (222011347)
 */

// Database yang dibuat baru 1 tabel, belum dilakukan normalisasi atau perlakuan lain
// yang mana akan menghemat dan mengefektifkan (?) tapi kayanya ga juga


public class Database implements Serializable{
    public static Database instance;
    
    private final String DB_TYPE = "mysql";
    private final String DB_HOST = "localhost";
    private final String DB_PORT = "3306";
    private final String DB_NAME = "kelempatdpp";
    private final String DB_USER = "root";
    private final String DB_PASS = "";
    
    public Database(){   
    }
    
    public static synchronized Database getInstance(){
        if(instance == null){
            instance = new Database();
        }
        return instance;
    }
    
    public void insertPerusahaan(idPerusahaan perusahaan)throws SQLException{
        Connection conn = getConnection();
        try{
            String sql = "INSERT INTO perusahaan VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);
//            pstmt.setString(1, perusahaan.getKodeProv());
//            pstmt.setString(2, perusahaan.getKodeKab());
//            pstmt.setString(3, perusahaan.getKodeKec());
//            pstmt.setString(4, perusahaan.getKodeKJU());
//            pstmt.setString(5, perusahaan.getNoUrut());

//          idPer = kode unik dari setiap perusahaan yang ada (gabungan dari beberapa kode yang ada pada KIP [refer ke kelas idPerusahaan])
            
            pstmt.setString(1, perusahaan.getIdPer());
            pstmt.setString(2, perusahaan.getNama());
            pstmt.setString(3, perusahaan.getAlamat());
            pstmt.setString(4, perusahaan.getNoTelp());
            pstmt.setString(5, perusahaan.getNoFaks());
            pstmt.setString(6, perusahaan.getBentuk());
            pstmt.setString(7, perusahaan.getInfoCacah());
            pstmt.setString(8, perusahaan.getKetCacah());
            pstmt.setString(9, perusahaan.getKegUsaha().getJenisUsahaUtama());
            pstmt.setString(10, perusahaan.getKegUsaha().getTanamanPangan());
            pstmt.setString(11, perusahaan.getKegUsaha().getHortikultura());
            pstmt.setString(12, perusahaan.getKegUsaha().getPerkebunan());
            pstmt.setString(13, perusahaan.getKegUsaha().getPeternakan());
            pstmt.setString(14, perusahaan.getKegUsaha().getKehutanan());
            pstmt.setString(15, perusahaan.getKegUsaha().getPerikanan());
            
            pstmt.executeUpdate();
        } catch(SQLException ex){
            throw ex;
        } finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public void deletePerusahaan(String idPer) throws SQLException{
        Connection conn = getConnection();
        try{
//            delete row Perusahaan berdasarkan id 
            String sql = "DELETE FROM perusahaan WHERE id_perusahaan = " +idPer;
            PreparedStatement pstmt = conn.prepareStatement(sql);
            
            pstmt.setString(1, idPer);
            pstmt.executeUpdate();
        }catch(SQLException ex){
            throw ex;
        }
        finally{
            if(conn != null){
                conn.close();
            }
        }
    }
    
    public List<idPerusahaan> selectPerusahaan() throws SQLException{
//      create a List of Perusahaan 
        List<idPerusahaan> perhsnList = new ArrayList<>();
       
        Connection conn = getConnection();
        
        try{
            String sql = "SELECT * FROM perusahaan";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
//              dibutuhkan constructor kosong pada kelas idPerusahaan
                idPerusahaan idPerhsn = new idPerusahaan();
                idPerhsn.setIdPer(rs.getString("id_perusahaan"));
                idPerhsn.setNama(rs.getString("nama"));
                idPerhsn.setAlamat(rs.getString("alamat"));
                idPerhsn.setNoTelp(rs.getString("no_telepon")); // nanti (semua var) dipastikan lagi mengenai penamaan variabel di databasenya
                idPerhsn.setNoFaks(rs.getString("no_faksimili"));
                idPerhsn.setBentuk(rs.getString("bentuk_badan_hukum"));
                idPerhsn.setInfoCacah(rs.getString("info_cacah"));
                idPerhsn.setKetCacah(rs.getString("ket_cacah"));
                

                idPerhsn.setKegUsaha();
                idPerhsn.getKegUsaha().setJenisUsahaUtama(rs.getString("jenis_usaha_utama"));
                idPerhsn.getKegUsaha().setHortikultura(rs.getString("hortikultura"));
                idPerhsn.getKegUsaha().setTanamanPangan(rs.getString("tanaman_pangan"));
                idPerhsn.getKegUsaha().setPerkebunan(rs.getString("perkebunan"));
                idPerhsn.getKegUsaha().setKehutanan(rs.getString("kehutanan"));
                idPerhsn.getKegUsaha().setPerikanan(rs.getString("perikanan"));

/*
//              object usaha perusahaan kosong untuk menggunakan setter pada idPerhsn.setUsahaPerusahaan                
                
                usahaPerusahaan ushPerhsn = new usahaPerusahaan();
                ushPerhsn.setJenisUsahaUtama(rs.getString("jenis_usaha_utama"));
                ushPerhsn.setTanamanPangan(rs.getString("tanaman_pangan"));
                ushPerhsn.setHortikultura(rs.getString("hortikultura"));
                ushPerhsn.setPerkebunan(rs.getString("perkebunan"));
                ushPerhsn.setKehutanan(rs.getString("kehutanan"));
                ushPerhsn.setPerikanan(rs.getString("perikanan"));
                
//              object ushPershn sudah terisi masing-masing variabelnya yg diambil dari database

//              setter pengambilan isi dari kelas ushPerhsn dari database untuk dimasukkan ke dalam array list
                idPerhsn.setKegUsaha(ushPerhsn);
*/                
                // menambahkan satu baris idPerusahaan beserta keseluruhan var nya ke dalam satu row pada List perusahaan
                // nanti bisa ditampilkan di tabel, pakai ini 
                perhsnList.add(idPerhsn);
            }// end of checking while if rs.next available
        } // end of try
        catch(SQLException ex){
            throw ex;
        }
        
        finally{
            if (conn != null){
                conn.close();
            }
        }
        return perhsnList;
    }
    
    public void editPerusahaan(idPerusahaan idPerusahaan, String idPer) throws SQLException {
        Connection conn = getConnection();
        try{
            String sql = "UPDATE perusahaan SET "
                    + "id_perusahaan = ?,"
                    + "nama = ?,"
                    + "alamat = ?,"
                    + "no_telepon = ?,"
                    + "no_faksimili = ?,"
                    + "bentuk_badan_hukum = ?,"
                    + "info_cacah = ?,"
                    + "ket_cacah = ?,"
                    + "jenis_usaha_utama = ?,"
                    + "tanaman_pangan = ?,"
                    + "holtikultura = ?,"
                    + "perkebunan = ?,"
                    + "peternakan = ?,"
                    + "kehutanan = ?,"
                    + "perikanan = ?";
            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, idPerusahaan.getIdPer());
            pstmt.setString(2, idPerusahaan.getNama());
            pstmt.setString(3, idPerusahaan.getAlamat());
            pstmt.setString(4, idPerusahaan.getNoTelp());
            pstmt.setString(5, idPerusahaan.getNoFaks());
            pstmt.setString(6, idPerusahaan.getBentuk());
            pstmt.setString(7, idPerusahaan.getInfoCacah());
            pstmt.setString(8, idPerusahaan.getKetCacah());
            pstmt.setString(9, idPerusahaan.getKegUsaha().getJenisUsahaUtama());
            pstmt.setString(10, idPerusahaan.getKegUsaha().getTanamanPangan());
            pstmt.setString(11, idPerusahaan.getKegUsaha().getHortikultura());
            pstmt.setString(12, idPerusahaan.getKegUsaha().getPerkebunan());
            pstmt.setString(13, idPerusahaan.getKegUsaha().getPeternakan());
            pstmt.setString(14, idPerusahaan.getKegUsaha().getKehutanan());
            pstmt.setString(15, idPerusahaan.getKegUsaha().getPerikanan());
            
            pstmt.executeUpdate();
        } catch (SQLDataException ex){
            throw ex;
        }
        finally{
            if(conn != null){
                conn.close();
            }
        }
        
    }
    
    public user getUser(String username)throws SQLException{
        user user = new user();
        Connection conn = getConnection();
        try{
            String sql = "SELECT * FROM `user` WHERE `username` = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, username);
            
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex){
            throw ex;
        } finally{
            if ((conn != null)) {
                conn.close();
            }
        }
        return user;
    }
    
    
    public Connection getConnection() throws SQLException{
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/kelempatdpp", "root","");
    }
    
}// end of class
