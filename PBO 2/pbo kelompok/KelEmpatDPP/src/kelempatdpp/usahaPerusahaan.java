/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package kelempatdpp;

/**
 * Anggota Kelompok:
 * 1. Anak Agung Gede Rai Bhaskara Darmawan Pemayun (222011343 / 08)
 * 2. Muhamad Feriyanto                             (222011347 / 09)
 * 3. Niken Yuliana                                 (222011685 / 30)
 * 4. Triana Nuramalia Shalli                       (222011703 / 31)
 * 5. Muhammad Naufal Faishal                       (222011739 / 33)
 * 
 * @author NIKEN YULIANA (222011685)
 */
public class usahaPerusahaan {
    private String jenisUsahaUtama;
    private String tanamanPangan;
    private String hortikultura;
    private String perkebunan;
    private String peternakan;
    private String kehutanan;
    private String perikanan;

    public usahaPerusahaan(String jenisUsahaUtama, String tanamanPangan, String holtikultura, String perkebunan, String peternakan, String kehutanan, String perikanan) {
        this.jenisUsahaUtama = jenisUsahaUtama;
        this.tanamanPangan = tanamanPangan;
        this.hortikultura = hortikultura;
        this.perkebunan = perkebunan;
        this.peternakan = peternakan;
        this.kehutanan = kehutanan;
        this.perikanan = perikanan;
    }

    public usahaPerusahaan() {
        
    }

    public String getJenisUsahaUtama() {
        return jenisUsahaUtama;
    }

    public void setJenisUsahaUtama(String jenisUsahaUtama) {
        this.jenisUsahaUtama = jenisUsahaUtama;
    }

    public String getTanamanPangan() {
        return tanamanPangan;
    }

    public void setTanamanPangan(String tanamanPangan) {
        this.tanamanPangan = tanamanPangan;
    }

    public String getHortikultura() {
        return hortikultura;
    }

    public void setHortikultura(String hortikultura) {
        this.hortikultura = hortikultura;
    }

    public String getPerkebunan() {
        return perkebunan;
    }

    public void setPerkebunan(String perkebunan) {
        this.perkebunan = perkebunan;
    }

    public String getPeternakan() {
        return peternakan;
    }

    public void setPeternakan(String peternakan) {
        this.peternakan = peternakan;
    }

    public String getKehutanan() {
        return kehutanan;
    }

    public void setKehutanan(String kehutanan) {
        this.kehutanan = kehutanan;
    }

    public String getPerikanan() {
        return perikanan;
    }

    public void setPerikanan(String perikanan) {
        this.perikanan = perikanan;
    }

}
