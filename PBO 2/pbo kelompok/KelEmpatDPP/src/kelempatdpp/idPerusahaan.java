/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package kelempatdpp;

/**
 * Anggota Kelompok:
 * 1. Anak Agung Gede Rai Bhaskara Darmawan Pemayun (222011343 / 08)
 * 2. Muhamad Feriyanto                             (222011347 / 09)
 * 3. Niken Yuliana                                 (222011685 / 30)
 * 4. Triana Nuramalia Shalli                       (222011703 / 31)
 * 5. Muhammad Naufal Faishal                       (222011739 / 33)
 * 
 * @author ANAK AGUNG GEDE RAI BHASKARA DARMAWAN PEMAYUN (222011343)
 */
public class idPerusahaan {
    private String kodeProv;
    private String kodeKab;
    private String kodeKec;
    private String kodeKJU;
    private String noUrut;
    private String nama;
    private String alamat;
    private String noTelp;
    private String noFaks;
    private String bentuk;
    private String infoCacah;
    private String ketCacah;
    private String idPer;
    private usahaPerusahaan kegUsaha;

    public idPerusahaan() {
    }

    public void setKodeProv(String kodeProv) {
        this.kodeProv = kodeProv;
    }

    public void setKodeKab(String kodeKab) {
        this.kodeKab = kodeKab;
    }

    public void setKodeKec(String kodeKec) {
        this.kodeKec = kodeKec;
    }

    public void setKodeKJU(String kodeKJU) {
        this.kodeKJU = kodeKJU;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public void setNoFaks(String noFaks) {
        this.noFaks = noFaks;
    }

    public void setBentuk(String bentuk) {
        this.bentuk = bentuk;
    }

    public void setInfoCacah(String infoCacah) {
        this.infoCacah = infoCacah;
    }

    public void setKetCacah(String ketCacah) {
        this.ketCacah = ketCacah;
    }

    public String getKodeProv() {
        return kodeProv;
    }

    public String getKodeKab() {
        return kodeKab;
    }

    public String getKodeKec() {
        return kodeKec;
    }

    public String getKodeKJU() {
        return kodeKJU;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public String getNoFaks() {
        return noFaks;
    }

    public String getBentuk() {
        return bentuk;
    }

    public String getInfoCacah() {
        return infoCacah;
    }

    public String getKetCacah() {
        return ketCacah;
    }
    
    public void setIdPer(){
        this.idPer = this.getKodeProv()+this.getKodeKab()+this.getKodeKec()+this.getKodeKJU();
    }
    
    public void setIdPer(String idPer){
        this.idPer = idPer;
    }
    
    public String getIdPer(){
        return idPer;
    }
    
    public void setKegUsaha(){
        this.kegUsaha = new usahaPerusahaan();
    }

    public usahaPerusahaan getKegUsaha() {
        return kegUsaha;
    }

}
