/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package kelEmpatDPP;

/**
 * Anggota Kelompok:
 * 1. Anak Agung Gede Rai Bhaskara Darmawan Pemayun (222011343 / 08)
 * 2. Muhamad Feriyanto                             (222011347 / 09)
 * 3. Niken Yuliana                                 (222011685 / 30)
 * 4. Triana Nuramalia Shalli                       (222011703 / 31)
 * 5. Muhammad Naufal Faishal                       (222011739 / 33)
 * 
 * @author MUHAMMAD NAUFAL FAISHAL (222011739)
 */
public class idKuesioner {
    private String provinsi;
    private String kab_kot;
    private String periode;

    public idKuesioner() {
    }
    
    public idKuesioner(String Provinsi, String kab_kot, String periode){
        this.provinsi = provinsi;
        this.kab_kot = kab_kot;
        this.periode = periode;
    }
    
    public void setProvinsi(String provinsi){
        this.provinsi = provinsi;
    }
    
    public String getProvinsi(){
        return provinsi;
    }
    
    public void setKab_kot(String kab_kot){
        this.kab_kot = kab_kot;
    }
    
    public String getKab_kot(){
        return kab_kot;
    }
    
    public void setPeriode(String periode){
        this.periode = periode;
    }
    
    public String getPeriode(){
        return periode;
    }
}
