/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package kelempatdpp;

import java.io.Serializable;

/**
 * Anggota Kelompok:
 * 1. Anak Agung Gede Rai Bhaskara Darmawan Pemayun (222011343 / 08)
 * 2. Muhamad Feriyanto                             (222011347 / 09)
 * 3. Niken Yuliana                                 (222011685 / 30)
 * 4. Triana Nuramalia Shalli                       (222011703 / 31)
 * 5. Muhammad Naufal Faishal                       (222011739 / 33)
 * 
 * @author TRIANA NURAMALIA SHALLI (222011703)
 */
public class user implements Serializable{
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
